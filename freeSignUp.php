<html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">  
      <title>util filmora</title>
      <link rel="stylesheet" type="text/css" href="css/index.css">
      <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse" id= "navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">
                        <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span>Sign In</a></li>
                </ul>
                </div>
            </div>
        </nav>
        <div class="Container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="Conta">Packages</li>
                </ol>
            </nav> 
        </div>
        <hr>
        <div class="Container">
            <h6 style="padding:0!important; font-size:10px!important;">STEP 1 OF 2</h6>
            <p>Choose a plan that suits you.</p>    
            <div class="row">
                <div style = "margin-left:560px" class="col-sm-6  col-md-offset-8">
                    <button class = "plansBtn">Basic</button>
                        <button style = "margin-left:120px"class = "plansBtn">Standard</button>
                    <button style = "margin-left:110px" class = "plansBtn">Premium</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-5">
                <span>Watch on your laptop, TV, phone and tablet</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                     <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
                <hr>
                
            <div class="row">
                <div class="col-sm-5">
                <span>HD available</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                     <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
                <hr>
            <div class="row">
                <div class="col-sm-5">
                <span>Ultra HD available</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                     <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-remove"></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
                <hr>
            <div class="row">
                <div class="col-sm-5">
                <span>Number of view screens per time</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                    <span><strong>2</strong></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span><strong>3</strong></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span><strong>5</strong></span>
                </div>
            </div>
                <hr>
            <div class="row">
                <div class="col-sm-5">
                <span>Unlimited movies and TV shows</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                     <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
                <hr>
            <div class="row">
                <div class="col-sm-5">
                <span>Cancel anytime</span>
                </div>
                <div class="col-sm-1  col-md-offset-1">
                     <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>  
                </div> 
                <div class="col-sm-1 col-md-offset-1">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
            <hr>
        </div>
        <div class="container text-center">
            <a href = "signUpStep3.php"><button class="galleria" id="freeRegfinal">Continue</button></a>
        </div>
        
        
        <footer class="container-fluid text-center">
            <p>&copy 2018. Utility Production</p>      
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>