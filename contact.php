   
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">  
  <title>util filmora</title>
  <link rel="stylesheet" type="text/css" href="css/index.css">
  
  <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->


  <!-- I'm using Max CDN because Ifeanyi said we could use what we were most comfortable with -->
  
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

</head>
<body>
    <nav class="navbar navbar-inverse" id= "navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">
                    <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                <li><a href="index.php">Home</a></li>
                <li><a href="Deals.php">Deals</a></li>
                <li   class="active"><a href="contact.php">Contact</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign In</a></li>
            </ul>
            
            </div>
        </div>
    </nav>        
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"> <a href ="Deals.php">Deals</a></li>
            <li class="breadcrumb-item active" aria-current="Conta">Contact</li>
            </ol>
        </nav>
        <br>
        <?php if (isset($_POST['firstname'])&& isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['country']) && isset($_POST['message'])):?>
            
            <?php
                include 'classes/contactUs.php';
                
                contactUs::process($_POST);
            ?>
                <div class="container" align="center">
                    <h1 style="color: #333!important; "> Thank you for your suggestion, You can now receive our free weekend movies!</h1>
                    <p style="font-size: 20px!important;">An email has been sent to you.</p>
                    <br>
                    <a href="contact.php">Contact Us</a>
                </div>
        <?php else: ?>
    
            <div style="text-align:center">
                <h2><strong>Contact Us</strong></h2>
                <p style="color: #333; font-size: 15px;">Please note: By giving a suggestion, you are subscribing to our
                newsletter</p>
            </div>
            <div class="row">         
                <div class="column">
                    <form action="contact.php" method="POST">
                        <label for="firstname" style="color: #333">First Name</label>
                        <input type="text" id="fname" class="form-control" name="firstname"
                               placeholder="Your first name.." required>
                        <small id="firstNameHelp" class="form-text
                            text-muted">Please fill in required entry </small><br>
                        <label for="lastname" style="color: #333">Last Name</label>
                        <input type="text" id="lname" class="form-control" name="lastname"
                            placeholder="Your last name.." required>
                        <small id="lastNameHelp" class="form-text
                            text-muted">Please enter your last name</small><br>
                        <label for="email" style="color: #333">Email Address</label>
                        <input type="email" id = "emm" class="form-control" name="email" size="50" 
                            placeholder="abc@xyz.com" required>
                        <small id="emailNameHelp" class="form-text
                            text-muted">Not a valid email address </small><br>
                        <label for="country" style="color: #333333">Country</label>
                        <select id="country"  class="form-control"name="country">
                            <option value="Nigeria">Nigeria</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Australia">Australia</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Brazil">Brazil</option>
                            <option value="Canada">Canada</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Egypt">Egypt</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Tunisia">Tunisia</option>
                        </select>
                        <small id="countryNameHelp" class="form-text
                            text-muted">We won't stalk you. This is just for paperwork</small><br>
                        <button id="showingS" class="galleria" type="button">I Have a Suggestion</button><br>
                        <div class="suggestTextArea" id="extraSuggest">
                            <label for="message">Subject</label>
                            <textarea id="subject" name="message"
                            placeholder="Write something.."
                            style="height:170px"></textarea>
                        </div>
                        <button id="submitter" type="submit" class="galleria">Submit</button> 
                    </form>
                </div>
            </div>
        <?php                endif;?>
    </div>

    
    <br>

    <footer class="container-fluid text-center">
        
        <p>&copy 2018. Utility Production</p>  
        
      </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
<script>

   $(".form-text").hide();
$(document).ready(function() {
    
    $("input").focus(function(){
        $(this).css("background-color", "#fffff0");
    });
    $("input").blur(function(){
        $(this).css("background-color", "#ffffff");
    });
    $(".suggestTextArea").hide();
    $("#showingS").click(function(){        
        $(".suggestTextArea").show(800);
        $(this).hide();
    });

    /*$("#submitter").click(function(){
        if ($("#fname").val() == ""){
            $("#firstNameHelp").show();
        }
        else ($("#firstNameHelp").hide());

        if ($("#lname").val() == ""){
            $("#lastNameHelp").show();
        }
        else   ($("#lastNameHelp").hide());

        if ($("#country").val() =="blank"){
            $("#countryNameHelp").show();
        }
        else   ($("#countryNameHelp").hide());

        var userInput = $("#emm").val();
        var pattern = /^[a-zA-Z0-9.!#$%&â€™*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if(!pattern.test(userInput)){
            $("#emailNameHelp").show();
        }
        else
            ($("#emailNameHelp").hide());
        
    });*/
});

</script>
</body>
</html>
