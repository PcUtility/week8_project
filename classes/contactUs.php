 <?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contactUs
 *
 * @author Okeugo
 */
class contactUs {
    
    public static function process($param){
        date_default_timezone_set('Africa/Lagos');
        
        /*
         |_________________________________________________________________________________
         | This code creates a csv file called contact.csv and the mode set to "a" append
         *________________________________________________________________________________ 
         */
        $file = fopen('contact.csv', 'a');
        
        //fputcsv($file, array('FirstName','LastName', 'Email', 'Country', 'Message', 'Date'));
        
        //well since i'm always confusing the american date format,  
        //date(DATE_RFC2822) would append something like this "Wed, 25 Sep 2013 15:28:57 -0700"
        fputcsv($file, array($param['firstname'],$param['lastname'],$param['email'],$param['country'],$param['message'],  date(DATE_RFC2822)));
        
        //close file stream
        fclose($file);
        
        //This prepares email message for the user
        $message = "Hi ".$param['firstname'].",\n\nWe appreciate your suggestion and we would look into it ASAP. \n\nWarm Regards,\nUtility Films.";
        //$headers = "Reply-To: Movie Admin <pc.okeugo@ulelulinks.com>\r\n";
        //this is a send mail function. it is a default php function
        mail($param['email'], "Utility Movies", $message); 
        
         /*
         |_________________________________________________________________________________
         | The following line of code is just to fulfill requirement specification 
          * thus: "The information should be retrieved from the file in which it was stored."
         *________________________________________________________________________________ 
         */
        $csv = array_map('str_getcsv', file('contact.csv'));
        echo $csv[sizeof($csv)-1] [0].'<br>';
        echo $csv[sizeof($csv)-1] [1].'<br>';
        echo $csv[sizeof($csv)-1] [2].'<br>';
        echo $csv[sizeof($csv)-1] [3].'<br>';
        echo $csv[sizeof($csv)-1] [4].'<br>';
        
    }
}
