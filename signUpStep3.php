
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">  
  <title>util filmora</title>
  <link rel="stylesheet" type="text/css" href="css/index.css">
  
  <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->


  <!-- I'm using Max CDN because Ifeanyi said we could use what we were most comfortable with -->
  
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

</head>
<body>
    <nav class="navbar navbar-inverse" id= "navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">
                    <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign In</a></li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"> <a href ="freeSignUp.php">Packages</a></li>
            <li class="breadcrumb-item active" aria-current="Conta">Sign up</li>
            </ol>
        </nav>
        <br>
        <div class="row">         
            <div class="column">
                <div>
                    <h6 style="padding:0!important; font-size:10px!important;">STEP 2 OF 2</h6>
                    <h2 style = "padding-top:0!important; margin-top: 0!important;">Sign up for free now</h2>
                    <p>Just this page and we're done! We all hate paperwork.</p>    
                </div>
                <form>

                    <label for="email" style="color: #333">Email Address</label>
                    <input type="email" id = "emm1" class="form-control" name="email" size="50" placeholder="abc@xyz.com"  required>
                    <small id="emailNameHelp"
                    class="form-text
                    text-muted">Not a valid email address </small><br>
                    <label for="userName"style="color: #333">Username</label>
                    <input type="text" id="uName1" class="form-control" name="userName" placeholder="James007" required>
                    <small id="lastNameHelp"
                    class="form-text
                    text-muted">Please enter a username</small><br>

                    <label for="passwrd" style="color: #333">Password</label>  
                    <input type="password" id="uName1" class="form-control" placeholder="Enter Password" name="passwrd" required>        
                    <button id="submitter" type="submit" class="galleria">Submit</button> 
                    <p>By creating an account you agree to our <a href="termsOfUse.html" target="_blank" style="color:dodgerblue">Terms & Privacy</a>.</p>
                </form>
            </div>
        </div>
    </div>

    
    <br>

    <footer class="container-fluid text-center">
        
        <p>&copy 2018. Utility Production</p>  
        
      </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
<script>

   $(".form-text").hide();
$(document).ready(function() {
    
    $("input").focus(function(){
        $(this).css("background-color", "#fffff0");
    });
    $("input").blur(function(){
        $(this).css("background-color", "#ffffff");
    });
    $(".suggestTextArea").hide();
    $("#showingS").click(function(){        
        $(".suggestTextArea").show(800);
        $(this).hide();
    });

    $("#submitter").click(function(){
      

        var userInput = $("#emm1").val();
        var pattern = /^[a-zA-Z0-9.!#$%&â€™*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if(!pattern.test(userInput)){
            $("#emailNameHelp").show();
        }
        else
            ($("#emailNameHelp").hide());
        
    });
});

</script>
</body>
</html>

